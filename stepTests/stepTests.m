//
//  stepTests.m
//  stepTests
//
//  Created by Yu KATO on 2013/11/09.
//  Copyright (c) 2013年 Yu KATO. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "StepApiClient.h"
#import <TRVSMonitor/TRVSMonitor.h>

@interface stepTests : XCTestCase

@end

@implementation stepTests

- (void)setUp
{
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown
{
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testExample
{
    StepApiClient *stepApiClient = [[StepApiClient alloc] init];
    XCTAssertNotNil(stepApiClient, @"APIクライアントが作られること");
    
    NSString *email = @"y.katsew+test@gmail.com";
    NSString *password = @"test1234";
    TRVSMonitor *monitor = [TRVSMonitor monitor];
    [stepApiClient getTokenByEmailAndPassword:email password:password];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didFailed:) name:StepApiClientDidFailedNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didSuccess:) name:StepApiClientDidSuccessNotification object:nil];
    [monitor waitWithTimeout:2.0];
    

    //XCTFail(@"No implementation for \"%s\"", __PRETTY_FUNCTION__);
}

- (void)didFailed:(NSNotification*) notification{
    [[TRVSMonitor monitor] signal];
}

- (void)didSuccess:(NSNotification*) notification{
    [[TRVSMonitor monitor] signal];
}

@end
