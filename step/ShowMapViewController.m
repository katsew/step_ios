//
//  ShowMapViewController.m
//  step
//
//  Created by Yu KATO on 2013/12/30.
//  Copyright (c) 2013年 Yu KATO. All rights reserved.
//

#import "ShowMapViewController.h"
#import "mapDetailViewController.h"
#import "CustomAnnotation.h"

@interface ShowMapViewController ()
@property (nonatomic, strong) CLLocation *currentLocation;
@property (nonatomic, strong) CLGeocoder *geocoder;
@property (nonatomic, strong) CLLocationManager *_locationManager;
@end

@implementation ShowMapViewController
@synthesize mapData;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.

    if([CLLocationManager locationServicesEnabled]){
        
        NSLog(@"Location Services Enabled");
        // Switch through the possible location
        // authorization states
        switch([CLLocationManager authorizationStatus]){
            case kCLAuthorizationStatusAuthorized:
                NSLog(@"We have access to location services");
                break;
            case kCLAuthorizationStatusDenied:
                NSLog(@"Location services denied by user");
                break;
            case kCLAuthorizationStatusRestricted:
                NSLog(@"Parental controls restrict location services");
                break;
            case kCLAuthorizationStatusNotDetermined:
                NSLog(@"Unable to determine, possibly not available");
        }
    }
    else{
        // locationServicesEnabled was set to NO
        NSLog(@"Location Services Are Disabled");
    }
    
    NSLog(@"----- show map data -----");
    NSLog(@"%@", mapData);
    
    MKCoordinateRegion region;
    region.span.latitudeDelta = 0.01;
    region.span.longitudeDelta = 0.01;
    double latitude = [mapData[@"lat"] doubleValue];
    double longitude = [mapData[@"lng"] doubleValue];
    region.center.latitude = latitude;
    region.center.longitude = longitude;
    
    [_showMapView setRegion:region animated:YES];
    
    CustomAnnotation* st = [[CustomAnnotation alloc] init];
    st.coordinate =  CLLocationCoordinate2DMake(latitude, longitude);
    st.title = mapData[@"name"];
    st.subtitle = mapData[@"desc"];
    [_showMapView addAnnotation:st];
    
}

- (void)startReceiveLocation {
    if (nil == self._locationManager) {
        self._locationManager = [[CLLocationManager alloc] init];
    }
    self._locationManager.delegate = self;
    [self._locationManager startUpdatingLocation];
}

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation {
    self.currentLocation = newLocation;
    
    //[self movePinLocationTo:self.currentLocation.coordinate];
    [self._locationManager stopUpdatingLocation];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
