//
//  TripTableViewCell.h
//  step
//
//  Created by Yu KATO on 2014/03/30.
//  Copyright (c) 2014年 Yu KATO. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TripTableViewCell : UITableViewCell

@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UILabel *dateLabel;
@property (nonatomic, strong) UIButton *editButton;

@end
