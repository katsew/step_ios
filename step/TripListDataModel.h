//
//  TripListDataModel.h
//  step
//
//  Created by Yu KATO on 2014/03/30.
//  Copyright (c) 2014年 Yu KATO. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TripListDataModel : NSObject
@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *startDate;
@property (nonatomic, strong) NSString *endDate;
@property (nonatomic, strong) NSString *date;
@property (nonatomic, assign) NSInteger contentID;

- (id)init;
- (id)initWithDictionary:(NSDictionary *)dic;
- (NSString *)joinDateStart:(NSString *)start andEnd:(NSString *)end;

@end
