//
//  ViewController.h
//  step
//
//  Created by Yu KATO on 2013/11/09.
//  Copyright (c) 2013年 Yu KATO. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <SVProgressHUD/SVProgressHUD.h>

@interface ViewController : UIViewController <UITextFieldDelegate, UIGestureRecognizerDelegate>
@property (strong, nonatomic) IBOutlet UITextField *email;
@property (strong, nonatomic) IBOutlet UITextField *password;
@property (weak, nonatomic) IBOutlet UITextField *passwordConfirm;
@property (weak, nonatomic) IBOutlet UIButton *buttonNewRegister;
@property (strong, nonatomic) UITapGestureRecognizer *touchOutsideKeyboard;

- (IBAction)newRegister:(id)sender;




@end
