//
//  CreateTripViewCell.m
//  step
//
//  Created by Yu KATO on 2014/04/06.
//  Copyright (c) 2014年 Yu KATO. All rights reserved.
//

#import "CreateTripViewCell.h"

@implementation CreateTripViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        _titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(70, 5, 250, 50)];
        [self addSubview:_titleLabel];
    }
    return self;
}

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
