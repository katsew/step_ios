//
//  CreateTripViewController.h
//  step
//
//  Created by Yu KATO on 2013/11/19.
//  Copyright (c) 2013年 Yu KATO. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import <MapKit/MapKit.h>
#import <AFNetworking/AFNetworking.h>
#import "GmapApiClient.h"


@interface CreateTripViewController : UIViewController <CLLocationManagerDelegate, UITextFieldDelegate, UITableViewDataSource, UITableViewDelegate>
{
    __weak IBOutlet UITextField *_searchTextField;
}
- (IBAction)searchMapButton:(id)sender;
- (IBAction)detectPlaceButton:(id)sender;
- (IBAction)cancelSearchButton:(id)sender;
@property (weak, nonatomic) IBOutlet UITableView *autoCompleteTableView;
@property (weak, nonatomic) IBOutlet MKMapView *mapView;
@property (strong, nonatomic) NSMutableArray *mapData;
@property (nonatomic, strong) NSArray *autoCompData;

@end
