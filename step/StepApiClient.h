//
//  StepApiClient.h
//  step
//
//  Created by Yu KATO on 2014/01/18.
//  Copyright (c) 2014年 Yu KATO. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AFNetworking/AFNetworking.h>
#import "StepUrlGenerator.h"


typedef void (^stepApiClientSuccessBlock)(AFHTTPRequestOperation *operation, id responseObject);
typedef void (^stepApiClientFailureBlock)(AFHTTPRequestOperation *operation, NSError *error);

@interface StepApiClient : NSObject

@property(nonatomic, strong) NSString *authToken;

+ (id)sharedClient;
- (BOOL)hasAuthToken;

- (void)stepLoginAPI:(NSString *)email
            password:(NSString *)password
        successBlock:(void (^)(AFHTTPRequestOperation *operation, id responseObject))successBlock
        failureBlock:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failureBlock;

- (void)stepNewRegisterationAPI:(NSString *)email
                       password:(NSString *)password
                   successBlock:(void (^)(AFHTTPRequestOperation *operation, id responseObject))successBlock
                   failureBlock:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failureBlock;


#pragma mark - GET/CREATE Trip
- (void)stepGetTripListAPI:(void (^)(AFHTTPRequestOperation *operation, id responseObject))successBlock
              failureBlock:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failureBlock;

- (void)stepGetContentAPI:(id)contentsID
             successBlock:(void (^)(AFHTTPRequestOperation *operation, id responseObject))successBlock
             failureBlock:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failureBlock;

- (void)stepCreateTripAPI:(NSDictionary *)postData
             successBlock:(void (^)(AFHTTPRequestOperation *operation, id responseObject))successBlock
             failureBlock:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failureBlock;

#pragma mark - UPDATE Trip
- (void)stepUpdateTripAPI:(NSDictionary *)postData
             successBlock:(void (^)(AFHTTPRequestOperation *operation, id responseObject))successBlock
             failureBlock:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failureBlock;

#pragma mark - DELETE Account/Trip
- (void)stepDeleteAccountAPI:(void (^)(AFHTTPRequestOperation *operation, id responseObject))successBlock
              failureBlock:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failureBlock;


- (void)stepDeleteTripAPI:(NSNumber *) tripID
             successBlock:(void (^)(AFHTTPRequestOperation *operation, id responseObject))successBlock
             failureBlock:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failureBlock;

@end
