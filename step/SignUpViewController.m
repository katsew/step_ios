//
//  ViewController.m
//  step
//
//  Created by Yu KATO on 2013/11/09.
//  Copyright (c) 2013年 Yu KATO. All rights reserved.
//

#import "SignUpViewController.h"
#import "StepApiClient.h"

@interface ViewController ()
@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.touchOutsideKeyboard = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onTouchOutside:)];
    self.touchOutsideKeyboard.delegate = self;
    self.touchOutsideKeyboard.numberOfTapsRequired = 1;
    [self.view addGestureRecognizer:self.touchOutsideKeyboard];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) onTouchOutside:(UIGestureRecognizer *)gestureRecognizer
{
    [self.email resignFirstResponder];
    [self.password resignFirstResponder];
    [self.passwordConfirm resignFirstResponder];
}

- (BOOL)newRegistrationShouldStart
{
    return (_email.text && [_password.text isEqualToString:_passwordConfirm.text] && [_password.text length] > 7);
}

- (BOOL) gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch
{
    if (gestureRecognizer == self.touchOutsideKeyboard) {
        if (self.email.isFirstResponder || self.password.isFirstResponder || self.passwordConfirm.isFirstResponder) {
            return YES;
        } else {
            return NO;
        }
    }
    return YES;
}

- (IBAction)newRegister:(id)sender {
    
    stepApiClientSuccessBlock successToSignUpBlock = ^(AFHTTPRequestOperation *operation, id responseObject) {

        [SVProgressHUD dismiss];
        [SVProgressHUD showSuccessWithStatus:@"登録完了"];
        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        UINavigationController* mainViewController = [mainStoryboard instantiateViewControllerWithIdentifier:@"LI_00"];
        [self presentViewController:mainViewController animated:YES completion:nil];
        
    };
    stepApiClientFailureBlock failedToSignUpBlock = ^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"ERROR: %@", error);
        [SVProgressHUD dismiss];
        [[[UIAlertView alloc] initWithTitle:@"登録失敗" message:[error localizedDescription] delegate:sender cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
    };
    if ([self newRegistrationShouldStart]) {

        [SVProgressHUD showWithStatus:@"登録中…" maskType:SVProgressHUDMaskTypeClear];
        
        NSString *email = _email.text;
        NSString *password = _password.text;
        
        NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
        [ud setObject:email forKey:@"email"];
        [ud setObject:password forKey:@"password"];
        [ud synchronize];
        
        NSLog(@"%@ : %@", [ud stringForKey:@"email"], [ud stringForKey:@"password"]);
        [[StepApiClient sharedClient] stepNewRegisterationAPI:email
                                                     password:password
                                                 successBlock:successToSignUpBlock
                                                 failureBlock:failedToSignUpBlock];
    } else {
        return;
    }

    
}



@end
