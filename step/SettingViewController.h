//
//  SettingViewController.h
//  step
//
//  Created by Yu KATO on 2014/01/26.
//  Copyright (c) 2014年 Yu KATO. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <SVProgressHUD/SVProgressHUD.h>
#import "StepApiClient.h"

@interface SettingViewController : UIViewController <UITableViewDelegate, UITableViewDataSource, UIAlertViewDelegate>
{
    
}

@property (weak, nonatomic) IBOutlet UITableView *settingTableView;
@property (nonatomic, strong) NSArray *settingViews;
typedef NS_ENUM(NSInteger, SettingPageDestination) {
    SettingLogout = 0,
    SettingChangeEmail,
    SettingChangePassword,
    SettingDeleteAccount
};
@property (nonatomic, strong) NSIndexPath *indexPathForAction;

@end
