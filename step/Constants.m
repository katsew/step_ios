//
//  Constants.m
//  step
//
//  Created by Yu KATO on 2014/01/11.
//  Copyright (c) 2014年 Yu KATO. All rights reserved.
//

#import "Constants.h"

#ifdef DEBUG
NSString *const kBASE_API_URL = @"http://step-in.herokuapp.com";
#else
NSString *const kBASE_API_URL = @"https://step-in.herokuapp.com";
#endif
NSInteger kAPI_VERSION = 1;

@implementation Constants

@end
