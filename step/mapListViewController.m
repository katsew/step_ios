//
//  mapListViewController.m
//  step
//
//  Created by Yu KATO on 2013/11/10.
//  Copyright (c) 2013年 Yu KATO. All rights reserved.
//

#import "mapListViewController.h"
#import "mapDetailViewController.h"
#import "StepApiClient.h"
#import "TripListDataModel.h"
#import "TripTableViewCell.h"
#import "CreateMapsViewController.h"

@interface mapListViewController ()

@property (nonatomic, strong)NSIndexPath *actionIndex;

@end

@implementation mapListViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.

    [self.tripListTableView setDelegate:self];
    [self.tripListTableView setDataSource:self];
    
    [_tripListTableView registerClass:[TripTableViewCell class] forCellReuseIdentifier:@"table_cell"];
    [_tripListTableView setEditing:NO];
    _tripListDic = [[NSMutableArray alloc] init];
    
    stepApiClientSuccessBlock successToLoginBlock = ^(AFHTTPRequestOperation *operation, id responseObject) {
        
        for (NSDictionary *dic in responseObject) {
            TripListDataModel *model = [[TripListDataModel alloc] initWithDictionary:dic];
            [_tripListDic addObject:model];
        }
        [self.tripListTableView reloadData];
        [SVProgressHUD showSuccessWithStatus:@"作成完了"];

    };
    stepApiClientFailureBlock failedToLoginBlock = ^(AFHTTPRequestOperation *operation, NSError *error) {

    };
    [[StepApiClient sharedClient] stepGetTripListAPI:successToLoginBlock
                                  failureBlock:failedToLoginBlock];
    [SVProgressHUD showWithStatus:@"しおりリスト作成中…" maskType:SVProgressHUDMaskTypeClear];

    
}

- (void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [_tripListTableView deselectRowAtIndexPath:[_tripListTableView indexPathForSelectedRow] animated:NO];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - UITableViewDelegate

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSLog(@"%lu", (unsigned long)_tripListDic.count);
    return [_tripListDic count];
}

- (BOOL) tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}

- (void) tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    return;
}

- (UITableViewCellEditingStyle) tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewCellEditingStyleNone;
}

- (BOOL) tableView:(UITableView *)tableView shouldIndentWhileEditingRowAtIndexPath:(NSIndexPath *)indexPath
{
    return NO;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    TripTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"table_cell" forIndexPath:indexPath];
    if (cell == nil) {
        cell = [[TripTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"table_cell"];
    }
    if ([_tripListDic count] != 0) {
        NSString *mapTitleText = [[_tripListDic objectAtIndex:indexPath.row] valueForKey:@"title"];
//        NSString *mapDateRange = [[_tripListDic objectAtIndex:indexPath.row] valueForKey:@"date"];
        [cell.titleLabel setText:mapTitleText];
//        [cell.dateLabel setText:mapDateRange];
        [cell.editButton addTarget:self action:@selector(pressedEditButton:) forControlEvents:UIControlEventTouchUpInside];
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UIStoryboard *showTripViewStoryboard = [UIStoryboard storyboardWithName:@"TripDetailView" bundle:nil];
    UIViewController *mapDetailViewController = [showTripViewStoryboard instantiateViewControllerWithIdentifier:@"VIEW_00"];
    [self.navigationController pushViewController:mapDetailViewController animated:YES];
    [[NSUserDefaults standardUserDefaults] setObject:[[_tripListDic objectAtIndex:indexPath.row] valueForKey:@"contentID"] forKey:@"current_contents_id"];
     ;
    [[NSUserDefaults standardUserDefaults] synchronize];
}

#pragma mark - UIAlertViewDelegate

- (void) performDeleteTrip
{
    stepApiClientSuccessBlock successToDeleteBlock = ^(AFHTTPRequestOperation *operation, id responseObject) {
        [_tripListDic removeObjectAtIndex:_actionIndex.row];
        [_tripListTableView deleteRowsAtIndexPaths:@[_actionIndex] withRowAnimation:UITableViewRowAnimationMiddle];
    };
    stepApiClientFailureBlock failedToDeleteBlock = ^(AFHTTPRequestOperation *operation, NSError *error) {
        [SVProgressHUD showErrorWithStatus:[error localizedDescription]];
    };
    
    [[StepApiClient sharedClient] stepDeleteTripAPI:[[_tripListDic objectAtIndex:_actionIndex.row] valueForKey:@"contentID"] successBlock:successToDeleteBlock failureBlock:failedToDeleteBlock];
}

- (void) performEditTrip
{
    
    [[NSUserDefaults standardUserDefaults] setObject:[[_tripListDic objectAtIndex:_actionIndex.row] valueForKey:@"contentID"] forKey:@"current_contents_id"];
    ;
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    UIStoryboard *createTripViewStoryboard = [UIStoryboard storyboardWithName:@"CreateTripView" bundle:nil];
    CreateMapsViewController* createTripViewController = [createTripViewStoryboard instantiateViewControllerWithIdentifier:@"MK_01"];
    createTripViewController.isReEdit = YES;
    [self presentViewController:createTripViewController animated:YES completion:nil];

}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    switch (buttonIndex) {
        case 1:
            [self performEditTrip];
            break;
        case 2:
            [self performDeleteTrip];
            break;
        default:
            break;
    }
}

#pragma mark - IBAction

- (IBAction)createNewTrip:(id)sender
{
    UIStoryboard *createTripViewStoryboard = [UIStoryboard storyboardWithName:@"CreateTripView" bundle:nil];
    UIViewController* createTripViewController = [createTripViewStoryboard instantiateViewControllerWithIdentifier:@"MK_00"];
    [self presentViewController:createTripViewController animated:YES completion:nil];
}

- (IBAction)presentSetting:(id)sender {
    UIStoryboard *settingStoryboard = [UIStoryboard storyboardWithName:@"SettingView" bundle:nil];
    UIViewController *settingViewController = [settingStoryboard instantiateViewControllerWithIdentifier:@"ST_00"];
    [self.navigationController pushViewController:settingViewController animated:YES];
}

- (void)pressedEditButton:(UIButton *)sender
{
    UITableViewCell *currentCell = (UITableViewCell *)sender.superview.superview;
    _actionIndex = [_tripListTableView indexPathForCell:currentCell];
    [[[UIAlertView alloc] initWithTitle:@"しおりの編集" message:@"しおりの操作を選択してください" delegate:self cancelButtonTitle:@"キャンセル" otherButtonTitles:@"編集", @"削除", nil] show];
}


@end

