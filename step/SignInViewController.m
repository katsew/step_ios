//
//  SignInViewController.m
//  step
//
//  Created by Yu KATO on 2013/11/10.
//  Copyright (c) 2013年 Yu KATO. All rights reserved.
//

#import "SignInViewController.h"
#import "Constants.h"

@interface SignInViewController ()

@end


@implementation SignInViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.touchOutsideKeyboard = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onTouchOutside:)];
    self.touchOutsideKeyboard.delegate = self;
    self.touchOutsideKeyboard.numberOfTapsRequired = 1;
    [self.view addGestureRecognizer:self.touchOutsideKeyboard];
    
}

- (void) onTouchOutside:(UIGestureRecognizer *)gestureRecognizer
{
    [self.email resignFirstResponder];
    [self.password resignFirstResponder];
}


- (BOOL) gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch
{
    if (gestureRecognizer == self.touchOutsideKeyboard) {
        if (self.email.isFirstResponder || self.password.isFirstResponder) {
            return YES;
        } else {
            return NO;
        }
    }
    return YES;
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)SignIn:(id)sender {
    
    NSString *email = _email.text;
    NSString *password = _password.text;
    
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    [ud setObject:email forKey:@"email"];
    [ud setObject:password forKey:@"password"];
    [ud synchronize];
    
    NSLog(@"%@ : %@", [ud stringForKey:@"email"], [ud stringForKey:@"password"]);
    
    stepApiClientSuccessBlock successToSignInBlock = ^(AFHTTPRequestOperation *operation, id responseObject) {

        [SVProgressHUD dismiss];
        [SVProgressHUD showSuccessWithStatus:@"ログイン完了"];
        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        UINavigationController* mainViewController = [mainStoryboard instantiateViewControllerWithIdentifier:@"LI_00"];
        [self presentViewController:mainViewController animated:YES completion:nil];

    };
    stepApiClientFailureBlock failedToSignInBlock = ^(AFHTTPRequestOperation *operation, NSError *error) {
        [SVProgressHUD dismiss];
        [[[UIAlertView alloc] initWithTitle:@"ログイン失敗" message:[error localizedDescription] delegate:sender cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
    };
    [SVProgressHUD showWithStatus:@"ログイン中…" maskType:SVProgressHUDMaskTypeClear];
    [[StepApiClient sharedClient] stepLoginAPI:email
                                      password:password
                                  successBlock:successToSignInBlock
                                  failureBlock:failedToSignInBlock];

}
@end
