//
//  GmapApiClient.h
//  step
//
//  Created by Yu KATO on 2014/01/25.
//  Copyright (c) 2014年 Yu KATO. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AFNetworking/AFNetworking.h>

typedef void (^gmapApiClientSuccessBlock)(AFHTTPRequestOperation *operation, id responseObject);
typedef void (^gmapApiClientFailureBlock)(AFHTTPRequestOperation *operation, NSError *error);

@interface GmapApiClient : NSObject

+ (id)sharedClient;

- (void)searchByTextAPI:(NSString *)searchText
           successBlock:(void (^)(AFHTTPRequestOperation *operation, id responseObject))successBlock
           failureBlock:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failureBlock;

- (void)autoCompleteAPI:(NSString *)searchText
           successBlock:(void (^)(AFHTTPRequestOperation *operation, id responseObject))successBlock
           failureBlock:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failureBlock;

@end
