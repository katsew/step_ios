//
//  TripTableViewCell.m
//  step
//
//  Created by Yu KATO on 2014/03/30.
//  Copyright (c) 2014年 Yu KATO. All rights reserved.
//

#import "TripTableViewCell.h"

@implementation TripTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        _titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(70, 5, 250, 50)];
        _dateLabel = [[UILabel alloc] initWithFrame:CGRectMake(70, 25, 200, 50)];
        _editButton = [[UIButton alloc] initWithFrame:CGRectMake(22, 22, 30, 30)];
        UIImage *iconEdit = [UIImage imageNamed:@"icon_edit.png"];
        UIImage *iconEditSelected = [UIImage imageNamed:@"icon_edit_selected.png"];
        [_editButton setBackgroundImage:iconEdit forState:UIControlStateNormal];
        [_editButton setBackgroundImage:iconEditSelected forState:UIControlStateSelected];
        [_editButton setBackgroundImage:iconEditSelected forState:UIControlStateHighlighted];
        [_editButton setBackgroundImage:iconEditSelected forState:UIControlStateReserved];
        [_dateLabel setLineBreakMode:NSLineBreakByTruncatingTail];
        [self addSubview:_titleLabel];
        [self addSubview:_dateLabel];
        [self addSubview:_editButton];
        [self setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
    }
    return self;
}

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
