//
//  AnnotationViewController.h
//  step
//
//  Created by Yu KATO on 2013/12/27.
//  Copyright (c) 2013年 Yu KATO. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>

@interface CustomAnnotation : NSObject <MKAnnotation>
{
    
}

@property (readwrite, nonatomic) CLLocationCoordinate2D coordinate;
@property (nonatomic, readwrite, copy) NSString *title;
@property (nonatomic, readwrite, copy) NSString *subtitle;

@end

