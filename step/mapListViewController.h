//
//  mapListViewController.h
//  step
//
//  Created by Yu KATO on 2013/11/10.
//  Copyright (c) 2013年 Yu KATO. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <SVProgressHUD/SVProgressHUD.h>

@interface mapListViewController : UIViewController <UITableViewDelegate, UITableViewDataSource, UIAlertViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tripListTableView;
@property (nonatomic, strong) NSMutableArray *tripListDic;
@property (nonatomic, strong) NSMutableArray *contentIdArray;

- (IBAction)createNewTrip:(id)sender;
- (IBAction)presentSetting:(id)sender;

@property (nonatomic) NSIndexPath *currentIndexPath;
@property (nonatomic, strong) NSNumber *tripID;

@end