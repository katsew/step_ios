//
//  AppDelegate.h
//  step
//
//  Created by Yu KATO on 2013/11/09.
//  Copyright (c) 2013年 Yu KATO. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "StepApiClient.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
