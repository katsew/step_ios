//
//  StepUrlGenerator.h
//  step
//
//  Created by Yu KATO on 2014/01/18.
//  Copyright (c) 2014年 Yu KATO. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface StepUrlGenerator : NSObject

#pragma mark - CreateOrDeleteAccountURL
+(NSString *)createAccountURL;
+(NSString *)deleteAccountURL;

#pragma mark - SettingAccountInfoURL
+(NSString *)resetPasswordURL;
+(NSString *)editPasswordURL;
+(NSString *)changeEmailURL;

#pragma mark - LoginOrLogoutURL
+(NSString *)logInURL;

#pragma mark - GetTripDataURL
+(NSString *)getTripListURL;
+(NSString *)getTripContentURL:(long)tripIndexNumber;

#pragma mark - CreateOrUpdateTripDataURL
+(NSString *)createTripURL;
+(NSString *)editTripURL;
+(NSString *)deleteTripURL;


@end
