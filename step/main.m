//
//  main.m
//  step
//
//  Created by Yu KATO on 2013/11/09.
//  Copyright (c) 2013年 Yu KATO. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
