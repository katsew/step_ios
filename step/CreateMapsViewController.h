//
//  CreateMapsViewController.h
//  step
//
//  Created by Yu KATO on 2013/12/28.
//  Copyright (c) 2013年 Yu KATO. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "StepApiClient.h"

@interface CreateMapsViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>{
    
}
- (IBAction)addPlaceButton:(id)sender;
- (IBAction)detectNameButton:(id)sender;
@property (retain, nonatomic) IBOutlet UITableView *mapDataTable;
@property (retain, nonatomic) NSMutableArray *mapDataForCell;
- (IBAction)SaveMapButton:(id)sender;
- (IBAction)CancelMapButton:(id)sender;
@property (weak, nonatomic) IBOutlet UITextField *enterTitleField;
@property (strong, nonatomic) IBOutlet UIView *saveMapView;

@property (nonatomic) BOOL isReEdit;

@end
