//
//  CreateTripViewController.m
//  step
//
//  Created by Yu KATO on 2013/11/19.
//  Copyright (c) 2013年 Yu KATO. All rights reserved.
//

#import "CreateTripViewController.h"
#import "CustomAnnotation.h"
#import "CreateMapsViewController.h"

@interface CreateTripViewController ()
@property (nonatomic, strong) CLLocation *currentLocation;
@property (nonatomic, strong) CLGeocoder *geocoder;
@property (nonatomic, strong) CLLocationManager *_locationManager;
@end

@implementation CreateTripViewController
@synthesize mapData;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    // Check to see if location services are enabled
    if([CLLocationManager locationServicesEnabled]){

       NSLog(@"Location Services Enabled");
       // Switch through the possible location
       // authorization states
       switch([CLLocationManager authorizationStatus]){
         case kCLAuthorizationStatusAuthorized:
           NSLog(@"We have access to location services");
           break;
         case kCLAuthorizationStatusDenied:
           NSLog(@"Location services denied by user");
           break;
         case kCLAuthorizationStatusRestricted:
           NSLog(@"Parental controls restrict location services");
           break;
         case kCLAuthorizationStatusNotDetermined:
           NSLog(@"Unable to determine, possibly not available");
       }
    }
    else{
        // locationServicesEnabled was set to NO
        NSLog(@"Location Services Are Disabled");
    }
    
    [_searchTextField setDelegate:self];
    [_autoCompleteTableView setHidden:YES];
    
    [_autoCompleteTableView setDataSource:self];
    [_autoCompleteTableView setDelegate:self];
    
    _mapView.showsUserLocation = YES;
    
    MKCoordinateRegion region;
    region.span.latitudeDelta = 0.01;
    region.span.longitudeDelta = 0.01;
    if(_mapView.userLocation){
        CLLocation* userloc = _mapView.userLocation.location;
        if(userloc){
            region.center.latitude = userloc.coordinate.latitude;
            region.center.longitude = userloc.coordinate.longitude;
        }
    }else{
        region.center.latitude = 35;
        region.center.longitude = 135;
    }
    
    [_mapView setRegion:region animated:NO];
    _mapView.userTrackingMode = MKUserTrackingModeFollow;
    
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}

- (void)startReceiveLocation {
    if (nil == self._locationManager) {
        self._locationManager = [[CLLocationManager alloc] init];
    }
    self._locationManager.delegate = self;
    [self._locationManager startUpdatingLocation];
}

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation {
    self.currentLocation = newLocation;
    
    //[self movePinLocationTo:self.currentLocation.coordinate];
    [self._locationManager stopUpdatingLocation];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)addAnnotationToMap:(NSDictionary *)mapListData
{
    double latitude, longitude;
    NSString *maptitle, *mapdesc;
    latitude = [mapListData[@"results"][0][@"geometry"][@"location"][@"lat"] doubleValue];
    longitude = [mapListData[@"results"][0][@"geometry"][@"location"][@"lng"] doubleValue];
    maptitle = mapListData[@"results"][0][@"name"];
    mapdesc = mapListData[@"results"][0][@"types"][0];
    MKCoordinateRegion region;
    region.span.latitudeDelta = 0.01;
    region.span.longitudeDelta = 0.01;
    region.center.latitude = latitude;
    region.center.longitude = longitude;
    
    [_mapView setRegion:region animated:YES];
    
    CustomAnnotation* st = [[CustomAnnotation alloc] init];
    st.coordinate =  CLLocationCoordinate2DMake(latitude, longitude);
    st.title = maptitle;
    st.subtitle = mapdesc;
    [_mapView addAnnotation:st];
    
    
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    NSArray *addedDatas = [ud arrayForKey:@"mapData"];
    NSString *token = [ud objectForKey:@"token"];
    NSLog(@"%@", token);
    long maxIndex = [addedDatas count];
    NSArray *key = [NSArray arrayWithObjects:
                    @"id",
                    @"name",
                    @"desc",
                    @"lat",
                    @"lng",
                    @"start_date",
                    @"end_date",
                    nil];
    NSArray *value = [NSArray arrayWithObjects:
                      [NSNumber numberWithInteger:maxIndex],
                      [NSString stringWithFormat:@"%@", maptitle],
                      [NSString stringWithFormat:@"%@", mapdesc],
                      [NSNumber numberWithDouble:latitude],
                      [NSNumber numberWithDouble:longitude],
                      [NSString stringWithFormat:@"2013-11-09 12:00:00 +0900"],
                      [NSString stringWithFormat:@"2013-11-09 12:00:00 +0900"],
                      nil];
    NSDictionary *content = [NSDictionary dictionaryWithObjects:value forKeys:key];
    mapData = [NSMutableArray array];
    NSLog(@"%@", content);
    
    
    for ( NSArray *array in addedDatas ){
        NSLog(@"SHOW ARRAY IN ADDED DATAS");
        NSLog(@"%@", array);
        NSLog(@"ADD ARRAY TO MAP_DATA");
        [mapData addObject:array];
        NSLog(@"%@", mapData);
    }
    [mapData addObject:content];
    NSLog(@"SHOW MAP DATA THAT NEWLY ADDED");
    NSLog(@"%@", mapData);
}

- (IBAction)searchMapButton:(id)sender {
    
    [_autoCompleteTableView setHidden:YES];

    [_searchTextField resignFirstResponder];
    NSLog(@"%@", _searchTextField.text);

    gmapApiClientSuccessBlock successToSearchBlock = ^(AFHTTPRequestOperation *operation, id responseObject) {
        NSDictionary *mapListData = [[NSDictionary alloc] initWithDictionary:responseObject];
        
        if ([mapListData[@"status"]  isEqual: @"INVALID_REQUEST"]){
            
            NSLog(@"INVALID_REQUEST");
            [[[UIAlertView alloc] initWithTitle:@"あら？" message:@"お探しの場所をいれてください" delegate:sender cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
            
        } else if ([mapListData[@"status"]  isEqual: @"ZERO_RESULTS"]){
            
            NSLog(@"ZERO_RESULTS");
            [[[UIAlertView alloc] initWithTitle:@"むむ" message:@"お探しの場所は見つかりませんでした" delegate:sender cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
            
        } else if ([mapListData[@"status"]  isEqual: @"OVER_QUERY_LIMIT"]){
            [[[UIAlertView alloc] initWithTitle:@"うっ" message:@"ちょっと苦しいです もう少し待ってください" delegate:sender cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
            
        } else if ([mapListData[@"status"]  isEqual: @"REQUEST_DENIED"]){
            [[[UIAlertView alloc] initWithTitle:@"ごめんなさい！" message:@"しばらくおやすみを頂きます" delegate:sender cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
            
        } else {
            
            [self addAnnotationToMap:mapListData];
        }
    };
    gmapApiClientFailureBlock failedToSearchBlock = ^(AFHTTPRequestOperation *operation, NSError *error) {

    };
    [[GmapApiClient sharedClient] searchByTextAPI:_searchTextField.text
                                             successBlock:successToSearchBlock
                                             failureBlock:failedToSearchBlock];
    
}

- (IBAction)detectPlaceButton:(id)sender {
    
    if (!mapData) {
        [[[UIAlertView alloc] initWithTitle:@"おや？" message:@"行きたい場所がわかりません！行きたい場所を検索してください" delegate:sender cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
        
    } else {
        NSLog(@"%@", mapData);
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        [defaults setObject:mapData forKey:@"mapData"];
        [defaults synchronize];
        [self performSegueWithIdentifier:@"detectPlace" sender:self];
    }

}

- (IBAction)cancelSearchButton:(id)sender {
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _autoCompData.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"autoCompItemCell" forIndexPath:indexPath];
    UILabel *autoCompText = (UILabel *)[cell viewWithTag:10];
    [autoCompText setText:[_autoCompData[indexPath.row] objectForKey:@"title"]];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {

    _searchTextField.text = [_autoCompData[indexPath.row] objectForKey:@"title"];
    [tableView deselectRowAtIndexPath:[tableView indexPathForSelectedRow] animated:YES];
}

- (void)tableView:(UITableView *)tableView didDeSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
}

- (void) textFieldDidEndEditing:(UITextField *)textField {

    [_searchTextField resignFirstResponder];
    NSLog(@"%@", _searchTextField.text);
    
    gmapApiClientSuccessBlock successToSearchBlock = ^(AFHTTPRequestOperation *operation, id responseObject) {
        NSDictionary *autoCompListData = [[NSDictionary alloc] initWithDictionary:responseObject];
        
        if ([autoCompListData[@"status"]  isEqual: @"INVALID_REQUEST"]){
            
            NSLog(@"INVALID_REQUEST");
            
        } else if ([autoCompListData[@"status"]  isEqual: @"ZERO_RESULTS"]){

            NSLog(@"ZERO_RESULTS");
            
        } else if ([autoCompListData[@"status"]  isEqual: @"OVER_QUERY_LIMIT"]){
            
            NSLog(@"OVER_QUERY_LIMIT");
            
        } else if ([autoCompListData[@"status"]  isEqual: @"REQUEST_DENIED"]){

            NSLog(@"REQUEST_DENIED");

        } else {
            
            NSMutableArray *array = [[NSMutableArray alloc] init];
            for (NSMutableDictionary *predictions in autoCompListData[@"predictions"]){

                // タイトルはtermsの配列の最初を抜き出したものにする
                // eg. 日本愛知県名古屋市中区栄３丁目２９−１ [サンマルクカフェ 名古屋パルコ店]
                NSString *title = [predictions[@"terms"] objectAtIndex:0][@"value"];

                // 候補のタイトルとその詳細のリファレンスを持っておく
                NSDictionary *dict = @{@"title" : title, @"reference" : predictions[@"reference"]};
                [array addObject:dict];
            }
            _autoCompData = array;
            [_autoCompleteTableView reloadData];
            [_autoCompleteTableView setHidden:NO];
        }
    };
    gmapApiClientFailureBlock failedToSearchBlock = ^(AFHTTPRequestOperation *operation, NSError *error) {
        
    };
    [[GmapApiClient sharedClient] autoCompleteAPI:_searchTextField.text
                                     successBlock:successToSearchBlock
                                     failureBlock:failedToSearchBlock];
    
}

@end
