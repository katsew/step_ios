//
//  StepApiClient.m
//  step
//
//  Created by Yu KATO on 2014/01/18.
//  Copyright (c) 2014年 Yu KATO. All rights reserved.
//

#import "StepApiClient.h"

@implementation StepApiClient

+ (id)sharedClient
{
    static id instance;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[self alloc] init];
        [instance setAuthToken: nil];
    });
    return instance;
}

- (BOOL)hasAuthToken{
    
    return [_authToken length] != 0;

}

- (void)stepLoginAPI:(NSString *)email
            password:(NSString *)password
        successBlock:(void (^)(AFHTTPRequestOperation *operation, id responseObject))successBlock
        failureBlock:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failureBlock
{

    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingMutableContainers];
    NSString *reqURL = [StepUrlGenerator logInURL];

    NSDictionary *params = @{@"email" : email, @"password" : password };
    NSLog(@"Parameters: %@", params);
    
    [manager POST:reqURL parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
        [self setAuthToken:responseObject[@"token"]];
        NSLog(@"%@", _authToken);
        if (successBlock) {
            successBlock(operation, responseObject);
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        NSLog(@"Operation: %@", operation);
        if (failureBlock) {
            failureBlock(operation, error);
        }
    }];
}

- (void)stepNewRegisterationAPI:(NSString *)email
                       password:(NSString *)password
                   successBlock:(void (^)(AFHTTPRequestOperation *operation, id responseObject))successBlock
                   failureBlock:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failureBlock
{
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingMutableContainers];
    NSString *reqURL = [StepUrlGenerator createAccountURL];
    
    NSDictionary *params = @{@"email" : email, @"password" : password };
    NSLog(@"Parameters: %@", params);
    
    [manager POST:reqURL parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSLog(@"JSON: %@", responseObject);
        [self setAuthToken:responseObject[@"token"]];
        NSLog(@"%@", _authToken);
        if (successBlock) {
            successBlock(operation, responseObject);
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        NSLog(@"Operation: %@", operation);
        if (failureBlock) {
            failureBlock(operation, error);
        }
    }];
}


#pragma mark - GET/CREATE Trip

- (void)stepGetTripListAPI:(void (^)(AFHTTPRequestOperation *operation, id responseObject))successBlock
              failureBlock:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failureBlock
{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingMutableContainers];
    NSString *reqURL = [StepUrlGenerator getTripListURL];
    
    NSDictionary *params = @{@"email" : [[NSUserDefaults standardUserDefaults] objectForKey:@"email"], @"token" : _authToken };
    NSLog(@"Parameters: %@", params);
    
    [manager GET:reqURL parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSLog(@"JSON: %@", responseObject);
        if (successBlock) {
            successBlock(operation, responseObject);
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        NSLog(@"Operation: %@", operation);
        if (failureBlock) {
            failureBlock(operation, error);
        }
    }];
}

- (void)stepCreateTripAPI:(NSMutableDictionary *)postData
             successBlock:(void (^)(AFHTTPRequestOperation *operation, id responseObject))successBlock
             failureBlock:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failureBlock
{
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    manager.responseSerializer = [AFJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingMutableContainers];

    NSString *reqURL = [StepUrlGenerator createTripURL];
    [postData setObject:_authToken forKey:@"token"];
    NSLog(@"Parameters: %@", postData);
    
    [manager POST:reqURL parameters:postData success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSLog(@"JSON: %@", responseObject);
        if (successBlock) {
            successBlock(operation, responseObject);
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        NSLog(@"Operation: %@", operation);
        if (failureBlock) {
            failureBlock(operation, error);
        }
    }];
}

- (void)stepGetContentAPI:(id)contentsID
             successBlock:(void (^)(AFHTTPRequestOperation *operation, id responseObject))successBlock
             failureBlock:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failureBlock
{
    
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingMutableContainers];
    NSLog(@"contentsID: %ld", (long)[contentsID integerValue]);
    
    NSString *reqURL = [StepUrlGenerator getTripContentURL:(long)[contentsID integerValue]];
    NSLog(@"REQUEST URL: %@", reqURL);
    
    NSDictionary *params = @{@"email" : [[NSUserDefaults standardUserDefaults] objectForKey:@"email"], @"token" : _authToken };
    NSLog(@"Parameters: %@", params);
    
    [manager GET:reqURL parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSLog(@"JSON: %@", responseObject);
        if (successBlock) {
            successBlock(operation, responseObject);
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        NSLog(@"Operation: %@", operation);
        if (failureBlock) {
            failureBlock(operation, error);
        }
    }];
}

#pragma mark - Update Trip Data

- (void)stepUpdateTripAPI:(NSMutableDictionary *)postData
             successBlock:(void (^)(AFHTTPRequestOperation *operation, id responseObject))successBlock
             failureBlock:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failureBlock
{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    manager.responseSerializer = [AFJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingAllowFragments];
    
    NSString *reqURL = [StepUrlGenerator editTripURL];
    [postData setObject:_authToken forKey:@"token"];
    NSLog(@"Parameters: %@", postData);
    
    [manager PUT:reqURL parameters:postData success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSLog(@"JSON: %@", responseObject);
        if (successBlock) {
            successBlock(operation, responseObject);
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        NSLog(@"Operation: %@", operation);
        if (failureBlock) {
            failureBlock(operation, error);
        }
    }];
}

#pragma mark - DeleteAccount

- (void)stepDeleteAccountAPI:(void (^)(AFHTTPRequestOperation *, id))successBlock
                failureBlock:(void (^)(AFHTTPRequestOperation *, NSError *))failureBlock
{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingMutableContainers];
    NSString *reqURL = [StepUrlGenerator deleteAccountURL];
    
    NSDictionary *params = @{@"email" : [[NSUserDefaults standardUserDefaults] objectForKey:@"email"], @"token" : _authToken };
    NSLog(@"Parameters: %@", params);
    
    [manager DELETE:reqURL parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSLog(@"JSON: %@", responseObject);
        if (successBlock) {
            successBlock(operation, responseObject);
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        NSLog(@"Operation: %@", operation);
        if (failureBlock) {
            failureBlock(operation, error);
        }
    }];
}

- (void)stepDeleteTripAPI:(NSNumber *) tripID
             successBlock:(void (^)(AFHTTPRequestOperation *operation, id responseObject))successBlock
             failureBlock:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failureBlock
{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingMutableContainers];
    NSString *reqURL = [StepUrlGenerator deleteTripURL];
    
    NSDictionary *params = @{@"email" : [[NSUserDefaults standardUserDefaults] objectForKey:@"email"], @"token" : _authToken, @"id" : tripID };
    NSLog(@"Parameters: %@", params);
    
    [manager DELETE:reqURL parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSLog(@"JSON: %@", responseObject);
        if (successBlock) {
            successBlock(operation, responseObject);
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        NSLog(@"Operation: %@", operation);
        if (failureBlock) {
            failureBlock(operation, error);
        }
    }];
}

@end
