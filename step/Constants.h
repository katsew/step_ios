//
//  Constants.h
//  step
//
//  Created by Yu KATO on 2014/01/11.
//  Copyright (c) 2014年 Yu KATO. All rights reserved.
//

#import <Foundation/Foundation.h>

extern NSString *const kBASE_API_URL;
extern NSInteger kAPI_VERSION;
@interface Constants : NSObject

@end
