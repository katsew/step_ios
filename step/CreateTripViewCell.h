//
//  CreateTripViewCell.h
//  step
//
//  Created by Yu KATO on 2014/04/06.
//  Copyright (c) 2014年 Yu KATO. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CreateTripViewCell : UITableViewCell

@property (nonatomic, strong) UILabel *titleLabel;

@end
