//
//  SignInViewController.h
//  step
//
//  Created by Yu KATO on 2013/11/10.
//  Copyright (c) 2013年 Yu KATO. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "StepApiClient.h"
#import <SVProgressHUD/SVProgressHUD.h>

@interface SignInViewController : UIViewController <UIGestureRecognizerDelegate>
@property (strong, nonatomic) UITapGestureRecognizer *touchOutsideKeyboard;

@property (strong, nonatomic) IBOutlet UITextField *email;
@property (strong, nonatomic) IBOutlet UITextField *password;

- (IBAction)SignIn:(id)sender;

@end
