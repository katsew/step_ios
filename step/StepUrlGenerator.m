//
//  StepUrlGenerator.m
//  step
//
//  Created by Yu KATO on 2014/01/18.
//  Copyright (c) 2014年 Yu KATO. All rights reserved.
//

#import "StepUrlGenerator.h"
#import "Constants.h"

@implementation StepUrlGenerator

#pragma mark - CreateOrDeleteAccountURL

// required: email, password
// return: token
+(NSString *)createAccountURL{
    return [NSString stringWithFormat:@"%@/api/v%ld/auth/sign_up", kBASE_API_URL, (long)kAPI_VERSION];
}

// required: email, token
+(NSString *)deleteAccountURL{
    return [NSString stringWithFormat:@"%@/api/v%ld/auth/cancel_account", kBASE_API_URL, (long)kAPI_VERSION];
}

#pragma mark - SettingAccountInfoURL
// required: email
+(NSString *)resetPasswordURL{
    return [NSString stringWithFormat:@"%@/api/v%ld/auth/reset_password", kBASE_API_URL, (long)kAPI_VERSION];
}

// required: reset_password_token, password
+(NSString *)editPasswordURL{
    return [NSString stringWithFormat:@"%@/api/v%ld/auth/edit_password", kBASE_API_URL,(long) kAPI_VERSION];
}
// required: email, token, new_email
+(NSString *)changeEmailURL{
    return [NSString stringWithFormat:@"%@/api/v%ld/auth/change_email", kBASE_API_URL, (long)kAPI_VERSION];
}

#pragma mark - LoginOrLogoutURL
// required: email, password
// return: token
+(NSString *)logInURL{
    return [NSString stringWithFormat:@"%@/api/v%ld/auth/sign_in", kBASE_API_URL, (long)kAPI_VERSION];
}

#pragma mark - GetTripDataURL
// required: email, token
// return: trip list
+(NSString *)getTripListURL{
    return [NSString stringWithFormat:@"%@/api/v%ld/trip/list", kBASE_API_URL, (long)kAPI_VERSION];
}
// required: email, token
// return: trip contents
+(NSString *)getTripContentURL:(long)tripIndexNumber{
    return [NSString stringWithFormat:@"%@/api/v%ld/trip/%ld", kBASE_API_URL, (long)kAPI_VERSION, (long)tripIndexNumber];
}

#pragma mark - CreateOrUpdateTripDataURL
// required email, token, title, contents, theme_id, start_date, end_date
+(NSString *)createTripURL{
    return [NSString stringWithFormat:@"%@/api/v%ld/trip/create", kBASE_API_URL, (long)kAPI_VERSION];
}
// required: email, token, id, title, contents, theme_id, start_date, end_date
+(NSString *)editTripURL{
    return [NSString stringWithFormat:@"%@/api/v%ld/trip/edit", kBASE_API_URL, (long)kAPI_VERSION];
}
// required: email, token, id
+(NSString *)deleteTripURL{
    return [NSString stringWithFormat:@"%@/api/v%ld/trip/delete", kBASE_API_URL, (long)kAPI_VERSION];
}

@end
