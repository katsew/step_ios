//
//  ShowMapViewController.h
//  step
//
//  Created by Yu KATO on 2013/12/30.
//  Copyright (c) 2013年 Yu KATO. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import <MapKit/MapKit.h>

@interface ShowMapViewController : UIViewController<CLLocationManagerDelegate, UITextFieldDelegate>{
    NSDictionary *mapData;
}
@property (nonatomic, copy) NSDictionary *mapData;
@property (weak, nonatomic) IBOutlet MKMapView *showMapView;


@end
