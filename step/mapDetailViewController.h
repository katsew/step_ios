//
//  mapDetailViewController.h
//  step
//
//  Created by Yu KATO on 2013/11/18.
//  Copyright (c) 2013年 Yu KATO. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <SVProgressHUD/SVProgressHUD.h>


@interface mapDetailViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UITableView *tripDetailTableView;
@property (nonatomic, strong) id contentsID;
@property (nonatomic, strong) NSArray *contentArray;
@property (nonatomic) NSInteger contentMaxNum;
    
@end
