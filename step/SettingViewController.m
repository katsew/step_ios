//
//  SettingViewController.m
//  step
//
//  Created by Yu KATO on 2014/01/26.
//  Copyright (c) 2014年 Yu KATO. All rights reserved.
//

#import "SettingViewController.h"
#import "StepApiClient.h"

@interface SettingViewController ()

@end

@implementation SettingViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.settingTableView setDelegate:self];
    [self.settingTableView setDataSource:self];
    
    _settingViews = @[@"ログアウト", @"メールアドレス変更", @"パスワード変更", @"アカウント削除"];
    
    
	// Do any additional setup after loading the view.
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _settingViews.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"settingItemCell" forIndexPath:indexPath];
    cell.textLabel.text = _settingViews[indexPath.row];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:[tableView indexPathForSelectedRow] animated:NO];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    _indexPathForAction = indexPath;
    switch (indexPath.row) {
        case SettingLogout:
            [[[UIAlertView alloc] initWithTitle:@"ログアウト" message:@"ログアウトしますか？" delegate:self cancelButtonTitle:@"キャンセル" otherButtonTitles:@"ログアウト", nil] show];
            break;
        case SettingDeleteAccount:
            [[[UIAlertView alloc] initWithTitle:@"アカウント削除" message:@"Stepアカウントを削除しますか？" delegate:self cancelButtonTitle:@"キャンセル" otherButtonTitles:@"削除", nil] show];
            break;
        default:
            break;
    }
    [tableView deselectRowAtIndexPath:[tableView indexPathForSelectedRow] animated:NO];
}

- (void)performLogout
{
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    [ud removeObjectForKey:@"email"];
    [ud removeObjectForKey:@"password"];
    [[StepApiClient sharedClient] setAuthToken:@""];
    
    UIStoryboard *beforeLoginStoryboard = [UIStoryboard storyboardWithName:@"BeforeLoginView" bundle:nil];
    UIViewController* beforeLoginView = [beforeLoginStoryboard instantiateViewControllerWithIdentifier:@"LO_00"];
    [self presentViewController:beforeLoginView animated:YES completion:nil];
}

- (void)performDeleteAccount
{

    stepApiClientSuccessBlock successToDeleteBlock = ^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
        [ud removeObjectForKey:@"email"];
        [ud removeObjectForKey:@"password"];
        [[StepApiClient sharedClient] setAuthToken:@""];
        
        [SVProgressHUD dismiss];
        UIStoryboard *beforeLoginStoryboard = [UIStoryboard storyboardWithName:@"BeforeLoginView" bundle:nil];
        UIViewController* beforeLoginView = [beforeLoginStoryboard instantiateViewControllerWithIdentifier:@"LO_00"];
        [self presentViewController:beforeLoginView animated:YES completion:^{
            [SVProgressHUD showSuccessWithStatus:@"アカウントを削除しました"];
        }];
        
    };
    stepApiClientFailureBlock failedToDeleteBlock = ^(AFHTTPRequestOperation *operation, NSError *error) {
        [SVProgressHUD dismiss];
        [SVProgressHUD showErrorWithStatus:[error localizedDescription]];
    };
    
    [SVProgressHUD showWithStatus:@"アカウント削除処理中" maskType:SVProgressHUDMaskTypeClear];
    [[StepApiClient sharedClient] stepDeleteAccountAPI:successToDeleteBlock
                                          failureBlock:failedToDeleteBlock];
    
    
}

- (void)detectCurrentActionAndPerform:(NSIndexPath *)indexPath
{
    switch (indexPath.row) {
        case SettingLogout:
            [self performLogout];
            break;
        case SettingDeleteAccount:
            [self performDeleteAccount];
            break;
        default:
            break;
    }
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    switch (buttonIndex) {
        case 1:
            [self detectCurrentActionAndPerform: _indexPathForAction];
            break;
        case 0:
        default:
            break;
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
