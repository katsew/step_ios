//
//  TripListDataModel.m
//  step
//
//  Created by Yu KATO on 2014/03/30.
//  Copyright (c) 2014年 Yu KATO. All rights reserved.
//

#import "TripListDataModel.h"

@implementation TripListDataModel

- (id)init {
    self = [super init];
    if (self) {
        
    }
    return self;
}

- (id)initWithDictionary:(NSDictionary *)dic {
    
    self.title = [dic objectForKey:@"title"];
    self.date = [self joinDateStart:[dic objectForKey:@"start_date"] andEnd:[dic objectForKey:@"end_date"]];
    self.startDate = [dic objectForKey:@"start_date"];
    self.endDate = [dic objectForKey:@"end_date"];
    self.contentID = [[dic objectForKey:@"id"] integerValue];

    return self;
}

- (NSString *)joinDateStart:(NSString *)start andEnd:(NSString *)end
{
    NSString *joinedDate = [NSString stringWithFormat:@"%@-%@", start, end];
    return joinedDate;
}

@end
