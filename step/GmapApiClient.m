//
//  GmapApiClient.m
//  step
//
//  Created by Yu KATO on 2014/01/25.
//  Copyright (c) 2014年 Yu KATO. All rights reserved.
//

#import "GmapApiClient.h"



@implementation GmapApiClient

NSString *const kBASE_GOOGLE_MAP_API_URL = @"https://maps.googleapis.com/maps/api";
NSString *const kGOOGLE_MAP_API_KEY = @"AIzaSyB9RwO4cUyH4eGfDjTrE6Bwhu8Z_37oXNg";

+ (id)sharedClient
{
    static id instance;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[self alloc] init];
    });
    return instance;
}

- (void)searchByTextAPI:(NSString *)searchText
           successBlock:(void (^)(AFHTTPRequestOperation *operation, id responseObject))successBlock
           failureBlock:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failureBlock
{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];

    NSString *reqURL = [NSString stringWithFormat:@"%@%@", kBASE_GOOGLE_MAP_API_URL, @"/place/textsearch/json"];
    NSLog(@"%@", reqURL);    
    NSDictionary *params = @{
                             @"sensor" : @"true",
                             @"query" : searchText,
                             @"key" : kGOOGLE_MAP_API_KEY
                            };
    NSLog(@"Parameters: %@", params);
    
    [manager GET:reqURL parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
        if (successBlock) {
            successBlock(operation, responseObject);
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        NSLog(@"Operation: %@", operation);
        if (failureBlock) {
            failureBlock(operation, error);
        }
    }];
    
}

- (void)autoCompleteAPI:(NSString *)searchText
           successBlock:(void (^)(AFHTTPRequestOperation *operation, id responseObject))successBlock
           failureBlock:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failureBlock
{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    
    NSString *reqURL = [NSString stringWithFormat:@"%@%@", kBASE_GOOGLE_MAP_API_URL, @"/place/autocomplete/json"];
    NSLog(@"%@", reqURL);
    NSDictionary *params = @{
                             @"sensor" : @"true",
                             @"input" : searchText,
                             @"key" : kGOOGLE_MAP_API_KEY
                             };
    NSLog(@"Parameters: %@", params);
    
    [manager GET:reqURL parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
        if (successBlock) {
            successBlock(operation, responseObject);
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        NSLog(@"Operation: %@", operation);
        if (failureBlock) {
            failureBlock(operation, error);
        }
    }];
    
}

@end
