//
//  AppDelegate.m
//  step
//
//  Created by Yu KATO on 2013/11/09.
//  Copyright (c) 2013年 Yu KATO. All rights reserved.
//

#import "AppDelegate.h"
#import "AFNetworkActivityLogger.h"

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    // Override point for customization after application launch.
    NSSetUncaughtExceptionHandler(&uncaughtExceptionHandler);
    [[AFNetworkActivityLogger sharedLogger] setLevel:AFLoggerLevelDebug];
    
    if ([self hasEmailAndPassword]) {
        [self showMainView];
    } else {
        [self showLoginView];
    }
    return YES;
}

- (BOOL)hasEmailAndPassword {
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
//    [ud removeObjectForKey:@"email"];
//    [ud removeObjectForKey:@"password"];
    return [ud objectForKey:@"email"] && [ud objectForKey:@"password"];
}

- (void)showMainView {

    stepApiClientSuccessBlock successToLoginBlock = ^(AFHTTPRequestOperation *operation, id responseObject) {
        
        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        UINavigationController* mainViewController = [mainStoryboard instantiateViewControllerWithIdentifier:@"LI_00"];
        self.window.rootViewController = mainViewController;
        [self.window makeKeyAndVisible];
        
    };
    stepApiClientFailureBlock failedToLoginBlock = ^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"ERROR: %@", error);
        [self showLoginView];
    };
    [[StepApiClient sharedClient] stepLoginAPI:[[NSUserDefaults standardUserDefaults] stringForKey:@"email"]
                                      password:[[NSUserDefaults standardUserDefaults] stringForKey:@"password"]
                                  successBlock:successToLoginBlock
                                  failureBlock:failedToLoginBlock];

}

-  (void)showLoginView {
    
    UIStoryboard *loginStoryboard = [UIStoryboard storyboardWithName:@"BeforeLoginView" bundle:nil];
    UINavigationController* loginViewController = [loginStoryboard instantiateViewControllerWithIdentifier:@"LO_00"];
    self.window.rootViewController = loginViewController;
    [self.window makeKeyAndVisible];
    
}

void uncaughtExceptionHandler(NSException *exception)
{
    // ここで、例外発生時の情報を出力します。
    NSLog(@"%@", exception.name);
    NSLog(@"%@", exception.reason);
    NSLog(@"%@", exception.callStackSymbols);
}
							
- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
