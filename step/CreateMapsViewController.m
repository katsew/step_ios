//
//  CreateMapsViewController.m
//  step
//
//  Created by Yu KATO on 2013/12/28.
//  Copyright (c) 2013年 Yu KATO. All rights reserved.
//

#import "CreateMapsViewController.h"
#import "CreateTripViewCell.h"
#import <SVProgressHUD/SVProgressHUD.h>

@interface CreateMapsViewController ()

@end

@implementation CreateMapsViewController
@synthesize mapDataForCell;
@synthesize mapDataTable;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.mapDataTable.dataSource = self;
    self.mapDataTable.delegate = self;
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if (_isReEdit) {
        
        [[StepApiClient sharedClient] stepGetContentAPI:[defaults objectForKey:@"current_contents_id"] successBlock:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            NSData *data = [responseObject[@"contents"] dataUsingEncoding:NSUTF8StringEncoding];
            mapDataForCell = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
            
            NSLog(@"%@", mapDataForCell);
            [SVProgressHUD showSuccessWithStatus:@"完了"];
            [self.mapDataTable reloadData];
            [defaults setBool:YES forKey:@"isReEdit"];
            [defaults synchronize];
            
        } failureBlock:^(AFHTTPRequestOperation *operation, NSError *error) {
            
            [SVProgressHUD showErrorWithStatus:@"失敗"];
            [self dismissViewControllerAnimated:YES completion:nil];
            
        }];
        [SVProgressHUD showWithStatus:@"しおり読み込み中…" maskType:SVProgressHUDMaskTypeClear];
        
    } else {
    
        mapDataForCell = [[defaults arrayForKey:@"mapData"] mutableCopy];
        
    }
    
    [mapDataTable registerClass:[CreateTripViewCell class] forCellReuseIdentifier:@"table_cell"];
    [mapDataTable setEditing:YES];
}

- (void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [mapDataTable deselectRowAtIndexPath:[mapDataTable indexPathForSelectedRow] animated:NO];
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return mapDataForCell.count;
}

- (BOOL) tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}

- (void) tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch (editingStyle) {
        case UITableViewCellEditingStyleDelete: {
            [mapDataForCell removeObjectAtIndex:indexPath.row];
            NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
            [ud setObject:mapDataForCell forKey:@"mapData"];
            [ud synchronize];
            [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationMiddle];
        }
            break;
        default:
            break;

    }
    
}

- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)sourceIndexPath toIndexPath:(NSIndexPath *)destinationIndexPath
{
    if (sourceIndexPath.row == destinationIndexPath.row) {
        return;
    }
    
    // 移動元から移動先へ配列を追加(コピー)する
    if (sourceIndexPath.row < destinationIndexPath.row) {
        [mapDataForCell insertObject:mapDataForCell[sourceIndexPath.row] atIndex:destinationIndexPath.row+1];
    } else {
        [mapDataForCell insertObject:mapDataForCell[sourceIndexPath.row] atIndex:destinationIndexPath.row];
    }

    // 移動元(コピー元)から配列を削除する
    if (sourceIndexPath.row < destinationIndexPath.row) {
        [mapDataForCell removeObjectAtIndex:sourceIndexPath.row];
    } else {
        [mapDataForCell removeObjectAtIndex:sourceIndexPath.row+1];
    }
    
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    [ud setObject:mapDataForCell forKey:@"mapData"];
    [ud synchronize];

}


- (UITableViewCellEditingStyle) tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewCellEditingStyleDelete;
}

- (BOOL) tableView:(UITableView *)tableView shouldIndentWhileEditingRowAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CreateTripViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"table_cell" forIndexPath:indexPath];
    
    if (cell == nil) {
        cell = [[CreateTripViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"table_cell"];
    }
    if ([mapDataForCell count] != 0) {
        NSString *mapTitleText = mapDataForCell[indexPath.row][@"name"];
        [cell.titleLabel setText:mapTitleText];
    }
    return cell;
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)addPlaceButton:(id)sender {
    
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    [ud setObject:mapDataForCell forKey:@"mapData"];
    [ud synchronize];
    [self performSegueWithIdentifier:@"addPlace" sender:self];
}

- (void)showViewWithAnimated {
    [self.view bringSubviewToFront:_saveMapView];
    [UIView animateWithDuration:0.5
                     animations:^{
                         _saveMapView.frame = CGRectMake(0, 0, 320, 640);
                     }];
}

- (void)hideViewWithAnimated {
    [_enterTitleField endEditing:YES];
    [UIView animateWithDuration:0.5
                     animations:^{
                         _saveMapView.frame = CGRectMake(0, 640, 320, 640);
                     }];
}

- (IBAction)detectNameButton:(id)sender {
    [self showViewWithAnimated];
    
}

- (IBAction)CancelMapButton:(id)sender {
    [self hideViewWithAnimated];
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [self.view endEditing:YES];
    return YES;
}

- (IBAction)SaveMapButton:(id)sender {
    
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    if (_isReEdit || [ud boolForKey:@"isReEdit"]) {
        
        [ud setObject:mapDataForCell forKey:@"mapData"];
        [ud synchronize];
        
        stepApiClientSuccessBlock successToUpdateBlock = ^(AFHTTPRequestOperation *operation, id responseObject) {
            
            [ud removeObjectForKey:@"mapData"];
            [ud removeObjectForKey:@"current_contents_id"];
            [ud removeObjectForKey:@"isReEdit"];
            
            UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            UINavigationController* mainViewController = [mainStoryboard instantiateViewControllerWithIdentifier:@"LI_00"];
            [self presentViewController:mainViewController animated:YES completion:nil];
            
        };
        stepApiClientFailureBlock failedToUpdateBlock = ^(AFHTTPRequestOperation *operation, NSError *error) {
            
        };
        [[StepApiClient sharedClient] stepUpdateTripAPI:[self createTripData]
                                           successBlock:successToUpdateBlock
                                           failureBlock:failedToUpdateBlock];
    } else {
        
        stepApiClientSuccessBlock successToCreateTripBlock = ^(AFHTTPRequestOperation *operation, id responseObject) {
            [ud removeObjectForKey:@"mapData"];
            UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            UINavigationController* mainViewController = [mainStoryboard instantiateViewControllerWithIdentifier:@"LI_00"];
            [self presentViewController:mainViewController animated:YES completion:nil];
            
        };
        stepApiClientFailureBlock failedToCreateTripBlock = ^(AFHTTPRequestOperation *operation, NSError *error) {
            
        };
        [[StepApiClient sharedClient] stepCreateTripAPI:[self createTripData]
                                           successBlock:successToCreateTripBlock
                                           failureBlock:failedToCreateTripBlock];
    }

}

- (NSMutableDictionary *)createTripData
{
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    NSData *testData = [NSJSONSerialization dataWithJSONObject:[ud arrayForKey:@"mapData"] options:NSJSONWritingPrettyPrinted error:nil];
    NSString *contentData = [[NSString alloc] initWithData:testData encoding:NSUTF8StringEncoding];
    NSArray *key = [NSArray arrayWithObjects:
                    @"email",
                    @"token",
                    @"title",
                    @"contents",
                    @"theme_id",
                    @"start_date",
                    @"end_date",
                    nil];
    NSArray *value = [NSArray arrayWithObjects:
                      [NSString stringWithFormat:@"%@", [ud stringForKey:@"email"]],
                      [NSString stringWithFormat:@""],
                      [NSString stringWithFormat:@"%@", _enterTitleField.text],
                      [NSString stringWithFormat:@"%@", contentData],
                      [NSNumber numberWithInteger:0],
                      [NSString stringWithFormat:@"2013-11-09 12:00:00 +0900"],
                      [NSString stringWithFormat:@"2013-11-09 12:00:00 +0900"],
                      nil];
    NSMutableDictionary *postData = [NSMutableDictionary dictionaryWithObjects:value forKeys:key];
    
    if (_isReEdit || [ud boolForKey:@"isReEdit"]) {
        [postData setObject:[ud objectForKey:@"current_contents_id"] forKey:@"id"];
    }
    
    return postData;
}


@end
