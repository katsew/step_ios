//
//  mapDetailViewController.m
//  step
//
//  Created by Yu KATO on 2013/11/18.
//  Copyright (c) 2013年 Yu KATO. All rights reserved.
//

#import "mapDetailViewController.h"
#import "mapListViewController.h"
#import "ShowMapViewController.h"
#import "StepApiClient.h"

@interface mapDetailViewController ()

@end

@implementation mapDetailViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    [self.tripDetailTableView setDelegate:self];
    [self.tripDetailTableView setDataSource:self];
    
    stepApiClientSuccessBlock successToGetContentBlock = ^(AFHTTPRequestOperation *operation, id responseObject) {
        

        NSData *data = [responseObject[@"contents"] dataUsingEncoding:NSUTF8StringEncoding];
        _contentArray = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil];
        NSLog(@"Map List Data: %@", _contentArray);
        
        [self.tripDetailTableView reloadData];
        [SVProgressHUD showSuccessWithStatus:@"完了"];
        
    };
    stepApiClientFailureBlock failedToGetContentBlock = ^(AFHTTPRequestOperation *operation, NSError *error) {
        [SVProgressHUD showErrorWithStatus:@"失敗"];
    };
    [[StepApiClient sharedClient] stepGetContentAPI:[[NSUserDefaults standardUserDefaults] objectForKey:@"current_contents_id"]
                                       successBlock:successToGetContentBlock
                                       failureBlock:failedToGetContentBlock];
    [SVProgressHUD showWithStatus:@"しおり読み込み中…" maskType:SVProgressHUDMaskTypeClear];
}

- (void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [_tripDetailTableView deselectRowAtIndexPath:[_tripDetailTableView indexPathForSelectedRow] animated:NO];
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSLog(@"%lu", (unsigned long)_contentArray.count);
    return _contentArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"tripItemCell" forIndexPath:indexPath];
    UILabel *tripPlace = (UILabel *)[cell viewWithTag:1];
    
    // タイトル
    NSString *mapTitleText = _contentArray[indexPath.row][@"name"];
    tripPlace.text = mapTitleText;
    return cell;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"showMapView"]) {
        ShowMapViewController *destinateView = [segue destinationViewController];
        NSIndexPath *indexPath = [_tripDetailTableView indexPathForSelectedRow];
        NSLog(@"%@", _contentArray[indexPath.row][@"name"]);
        destinateView.mapData = _contentArray[indexPath.row];
    }
}

@end
